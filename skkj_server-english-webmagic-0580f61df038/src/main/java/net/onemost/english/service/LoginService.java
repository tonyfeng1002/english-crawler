package net.onemost.english.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;

public class LoginService{
	
	private HttpPost post = null;
	
	public String login() throws ClientProtocolException, IOException {
		
		//获取SESSIONID
		CookieStore cookieStore = new BasicCookieStore();
		HttpClient httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
		
		post = new HttpPost("https://www.initwords.com");
		
		post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		post.setHeader("Accept-Encoding", "gzip, deflate, br");
		post.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
		post.setHeader("Cache-Control", "no-cache");
		post.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.119 Safari/537.36");
		post.setHeader("Referer", "https://www.initwords.com/studycenter/studyhome.jsp");
		post.setHeader("Origin", "https://www.initwords.com");
		
		httpClient.execute(post);
		
		Cookie cookie = null;
		
		List<Cookie> cookies = cookieStore.getCookies();
		for (Cookie cookie2 : cookies) {
			if("JSESSIONID".equals(cookie2.getName())) {
				cookie = cookie2;
			}
		}
		
		if(cookie == null) {
			throw new RuntimeException("session获取失败");
		}
		
		return cookie.getValue();
	}
	
}
