package net.onemost.english.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import net.onemost.english.dto.LoginInfo;
import us.codecraft.webmagic.Page;

public class GetCode {

	public static LoginInfo getInfo(Page page) {
		JSONObject jsonObject = JSON.parseObject(page.getJson().toString());
		
		jsonObject = jsonObject.getJSONObject("data");
		
		if(jsonObject == null) {
			throw new RuntimeException("登录失败");
		}
		
		String tokenCode = jsonObject.getString("tokenCode");
		
		jsonObject = jsonObject.getJSONObject("user");
		
		String userCode = jsonObject.getString("userCode");
		
		LoginInfo loginInfo = new LoginInfo();
		loginInfo.setTokenCode(tokenCode);
		loginInfo.setUserCode(userCode);
		return loginInfo;
	}
	
}
