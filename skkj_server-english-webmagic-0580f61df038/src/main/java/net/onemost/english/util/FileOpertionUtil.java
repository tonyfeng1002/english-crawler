package net.onemost.english.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class FileOpertionUtil {

	/**
	 * 查询是否存配置文件
	 * @return
	 */
	public static boolean getLoginFile() {
		File file = new File("D:\\jrm\\login.txt");
		
		if(!file.exists()) {
			return false;
		}
		
		return false;
	}
	
	/**
	 * 生成配置文件
	 * @param jsesseionId
	 * @param userCode
	 * @param tokenCode
	 * @throws IOException
	 */
	public static void createLoginFile(String jsesseionId, String userCode, String tokenCode) throws IOException {
		File file = new File("D:\\jrm\\login.txt");
		if(file.exists()) {
			file.delete();
		}
		
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file));
		
		printWriter.println(jsesseionId);
		printWriter.println(tokenCode);
		printWriter.println(userCode);
		
		printWriter.flush();
		printWriter.close();
		
	}
	
	/**
	 * 获得配置
	 * @return
	 * @throws IOException
	 */
	public static Map<String, String> getConfig() throws IOException{
		File file = new File("D:\\jrm\\login.txt");
		BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
		Map<String, String> map = new HashMap<>();
		map.put("session", bufferedReader.readLine());
		map.put("userCode", bufferedReader.readLine());
		map.put("tokenCode", bufferedReader.readLine());
		bufferedReader.close();
		return map;
	}
	
}
