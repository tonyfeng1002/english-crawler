package net.onemost.english.model;

import java.beans.Transient;

public class Book {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.id
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.book_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String bookName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.model_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String modelName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.book_code
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String bookCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.introduce
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String introduce;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.totalUnitNbr
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private Integer totalunitnbr;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.publisher
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String publisher;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.book_group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String bookGroupName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column book.group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    private String groupName;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.id
     *
     * @return the value of book.id
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.id
     *
     * @param id the value for book.id
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.book_name
     *
     * @return the value of book.book_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.book_name
     *
     * @param bookName the value for book.book_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.model_name
     *
     * @return the value of book.model_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getModelName() {
        return modelName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.model_name
     *
     * @param modelName the value for book.model_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.book_code
     *
     * @return the value of book.book_code
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getBookCode() {
        return bookCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.book_code
     *
     * @param bookCode the value for book.book_code
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.introduce
     *
     * @return the value of book.introduce
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getIntroduce() {
        return introduce;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.introduce
     *
     * @param introduce the value for book.introduce
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.totalUnitNbr
     *
     * @return the value of book.totalUnitNbr
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public Integer getTotalunitnbr() {
        return totalunitnbr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.totalUnitNbr
     *
     * @param totalunitnbr the value for book.totalUnitNbr
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setTotalunitnbr(Integer totalunitnbr) {
        this.totalunitnbr = totalunitnbr;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.publisher
     *
     * @return the value of book.publisher
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.publisher
     *
     * @param publisher the value for book.publisher
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.book_group_name
     *
     * @return the value of book.book_group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getBookGroupName() {
        return bookGroupName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.book_group_name
     *
     * @param bookGroupName the value for book.book_group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setBookGroupName(String bookGroupName) {
        this.bookGroupName = bookGroupName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column book.group_name
     *
     * @return the value of book.group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column book.group_name
     *
     * @param groupName the value for book.group_name
     *
     * @mbggenerated Fri Mar 16 13:38:31 GMT+08:00 2018
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
    private Integer startNum;

	public Integer getStartNum() {
		return startNum;
	}

	public void setStartNum(Integer startNum) {
		this.startNum = startNum;
	}
    
    
}